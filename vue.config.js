const fs = require('fs')
const packageJson = fs.readFileSync('./package.json')
const version = JSON.parse(packageJson).version || 'UNKNOWN_VERSION'
const webpack = require('webpack')

module.exports = {
    lintOnSave: false,
    configureWebpack: {
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    VUE_APP_VERSION: '"' + version + '"'
                }
            })
        ]
    }
}