import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

import BoardsView from './components/boards/BoardsView.vue'
import ListView from './components/lists/ListView.vue'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(Vuex)

const routes = [
  { path: '/', component: BoardsView },
  { path: '/board/:id', component: ListView }
];

const store = new Vuex.Store({
  state: {
    sessionToken: null,
    boardLabels: []
  },
  mutations: {
    login (state, payload) {
      state.sessionToken = payload.sessionToken;
    },
    newBoardLabels (state, labels) {
      state.boardLabels = labels;
    }
  }
})

var router = new VueRouter({
  routes
});

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
}).$mount('#app')
